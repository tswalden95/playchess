GDC Chess
	
Project to make a online chess game.

Contact Peter Sassaman with Questions peter.sassaman@gmail.com


Concerning Class Headers
If you make / edit a class it should look as follows

/* Description
 * 
 * @author                  <Author Name>
 * @last person to update   <Last Person To Update>
 * @version                 <Major/Minor/Patch>
 * @see also		    <Closely Related Classes>
 * 
 */

as for Major/Minor/Patch there is probably some ambiguity so I have decided to interpret it as follows.
Major - You added a new method or even class to the file.  This does not mean make the Major 10 just because you added ten Methods.  If you add a group
	of classes and methods in one work period just increase by one.
Minor - You reworked code in a Meathod to add new functionality or fixed a major bug that required changing a sizeable amount of code.
Patch - You fixed a bug without reworking too much code.

on the last two use discretion