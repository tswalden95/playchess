﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;



public class Client : MonoBehaviour 
{
    static String message;
    GameObject GameManager;
    public GameObject Camera0;
    public GameObject Camera1;
    bool Cam0 = false;
    bool Cam1 = false;
    bool Black = false;
    bool oneSkip = false;
    bool leaveRoom = false;
    int[] responseData3 = {-1, -1, -1};
    bool isAlive = true;
    Thread clientThread;
    NetworkStream stream;

    void Start()
    {
        GameManager = GameObject.Find("GameManager");
        message = "TEST";
        clientThread = new Thread(Connect);
        clientThread.IsBackground = true;
        clientThread.Start();
    }
    void Update()
    {
        if (leaveRoom == true)
            SceneManager.LoadScene("ChessMenu");
        if (Cam0)
        {
            Camera0.SetActive(true);
        }  
        if (Cam1)
        {
            Camera1.SetActive(true);
            Co.Player = true;
        } 
        if(responseData3[0] != -1 && responseData3[1] != -1 && responseData3[2] != -1)
        {
            Piece tempPiece = GameManager.GetComponent<GameManagement>().GetPiece(responseData3[0]).GetComponent<Piece>();
            tempPiece.MovePiece(responseData3[1], responseData3[2]);
            
            
            if(GameManager.GetComponent<GameManagement>().GetPiece(responseData3[0]).name.Contains("King"))
                if(tempPiece.firstTurn && responseData3[2]==2)
                {
                    Piece tempRook = GameObject.Find("GameManager").GetComponent<GameManagement>().GetAllPieces(tempPiece.GridX, 0).GetComponent<Piece>();
                    if(tempRook.firstTurn)
                        tempRook.MovePiece2(tempPiece.GridX, tempPiece.GridY + 1);
                    tempRook.firstTurn = false;
                }    
                else if (tempPiece.firstTurn && responseData3[2]==6)
                {
                    Piece tempRook = GameObject.Find("GameManager").GetComponent<GameManagement>().GetAllPieces(tempPiece.GridX, 7).GetComponent<Piece>();
                    if(tempRook.firstTurn)
                        tempRook.MovePiece2(tempPiece.GridX, tempPiece.GridY - 1);
                    tempRook.firstTurn = false;
                }
            tempPiece.firstTurn = false;
            responseData3[0] = -1;
            responseData3[1] = -1;
            responseData3[2] = -1;
        }
        
    }
    private void Connect()
    {
        String server = IPAdd.GetIP();
        //String server = "192.168.0.11";
        try
        {
            //Debug.Log("ClientStarted");
            // Create a TcpClient.
            // Note, for this client to work you need to have a TcpServer 
            // connected to the same address as specified by the server, port
            // combination.
            Int32 port = 1991;
            TcpClient client = new TcpClient(server, port);
            
            // Translate the passed message into ASCII and store it as a Byte array.
            Byte[] data;
            //NetworkStream stream;
            // Get a client stream for reading and writing.
            //  Stream stream = client.GetStream();
           stream = client.GetStream();
           
            
            //Debug.Log("PreWhile");
            while(isAlive)
            {
                if(!oneSkip)
                {
                    data = System.Text.Encoding.ASCII.GetBytes(message);
                    // Send the message to the connected TcpServer. 
                    stream.Write(data, 0, data.Length);
                    String tempString1 = ("Sent: " + message);

                    //Debug.Log(tempString1);
                    
                }
                

                // Receive the TcpServer.response.

                // Buffer to store the response bytes.
                data = new Byte[256];

                // String to store the response ASCII representation.
                String responseData = String.Empty;

                // Read the first batch of the TcpServer response bytes.
                Int32 bytes = stream.Read(data, 0, data.Length);
                responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                String tempString2 = "Received: " + responseData + "$";
                //Debug.Log(tempString2);
                if (responseData == "Camera0")
                {
                    Cam0 = true;
                }
                else if (responseData == "Camera1")
                {
                    Cam1 = true;
                    oneSkip = true;
                }
                else if (responseData == "close")
                {
                    break;
                }
                else
                {
                    String[] responseData2 = responseData.Split(',');
                    responseData3 = Array.ConvertAll<string, int>(responseData2, int.Parse);
                    //Debug.Log("Response");
                    //Debug.Log(responseData3[0].ToString());
                    oneSkip = false;
                }
                //Debug.Log(tempString2);
                if(!oneSkip)
                {
                    message = "";
                    while (message == "")
                    {
                        Thread.Sleep(100);
                    }
                }
                
        }
            stream.Close();
            client.Close();
        }
        catch (ArgumentNullException e)
        {
            string tempString3 = "ArgumentNullException: " + e;
            Console.WriteLine(tempString3);
            leaveRoom = true;
        }
        catch (SocketException e)
        {
            string tempString4 = "SocketException: " + e;
            Console.WriteLine(tempString4);
            leaveRoom = true;
        }

        Console.WriteLine("\n Press Enter to continue...");
        Console.Read();
    }
    public static void SetMessage(String imessage)
    {
        message = imessage;
    }
    IEnumerator ClientWait()
    {
        yield return new WaitForSeconds(1);
    }
    void OnApplicationQuit()
    {
        stream.Close();
        clientThread.Abort();
        clientThread.Join();
        Application.Quit();
    }

    public void QuitChess()
    {
        stream.Close();
        clientThread.Abort();
        clientThread.Join();
        SceneManager.LoadScene("ChessMenu");
    }

    // Update is called once per frame

}



