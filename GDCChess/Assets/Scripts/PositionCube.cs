﻿using UnityEngine;
using System.Collections;

public class PositionCube : MonoBehaviour
{
    Rigidbody myRigidBody;
    int Gridx, Gridy;
    int castleFlag = 0;

    void Start()
    {
        //castleFlag = 0;
        myRigidBody = this.GetComponent<Rigidbody>();
    }

    void OnMouseDown()
    {
        //if (castleFlag > 0)
            Debug.Log("Casle");
            Debug.Log(castleFlag);
        try
        {
            Piece tempCoordinates = this.gameObject.GetComponentInParent<Piece>();
            Debug.Log("GotCoords");
            if(castleFlag == 1)
                GameObject.Find("GameManager").GetComponent<GameManagement>().GetAllPieces(Gridx, 0).GetComponent<Piece>().MovePiece2(Gridx, Gridy + 1);
            if(castleFlag == 2)
                GameObject.Find("GameManager").GetComponent<GameManagement>().GetAllPieces(Gridx, 7).GetComponent<Piece>().MovePiece2(Gridx, Gridy - 1);
            tempCoordinates.MovePiece(Gridx, Gridy);
        }
        catch 
        { 
        }
    }

    

    public void SetCoords(int Gridx, int Gridy)
    {
        this.Gridx = Gridx;
        this.Gridy = Gridy;
    }

    public void SetCoords(int Gridx, int Gridy, int castleFlag)
    {
        //Debug.Log("EnterCasle");
        this.Gridx = Gridx;
        this.Gridy = Gridy;
        this.castleFlag = castleFlag;
        //Debug.Log(castleFlag);
    }

}
