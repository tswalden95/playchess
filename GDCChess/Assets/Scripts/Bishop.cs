﻿using UnityEngine;
using System.Collections;

/* Main class for controlling Bishops In the Scene View
 * 
 * @author                  Peter Sassaman
 * @last person to update   Peter Sassaman
 * @version                 0.0.0         
 * @see also                Coordinates Client  GameManagement
 * 
 */

public class Bishop : Piece
{
    /*  Controls what hapens when Bishop is clicked in the scene.
     * 
     * @param   void
     * @return  void
     */
    void OnMouseDown()
    {
        //  make position cubes
        if (((Co.Player && GameManagement.GetTurn() && color == "BLACK") || (!Co.Player && !GameManagement.GetTurn() && color == "WHITE")) && !GameManagement.GetAlreadyClicked())
        {
            PlacePositionCubesInRow("NORTHEAST",8,0);
            PlacePositionCubesInRow("NORTHWEST",8,0);
            PlacePositionCubesInRow("SOUTHEAST",8,0);
            PlacePositionCubesInRow("SOUTHWEST",8,0);
        }
    }
}
