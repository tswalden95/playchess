﻿using UnityEngine;
using System.Collections;
using System.Net;

public class IPAdd : MonoBehaviour {

    static string IPAddressToConnectTo;
    void Start()
    {
        DontDestroyOnLoad(transform.gameObject);
    }
    
    public void SetIP(string iIP)
    {
        IPAddressToConnectTo = iIP;
    }
    public static string GetIP()
    {
        return IPAddressToConnectTo;
    }
}
