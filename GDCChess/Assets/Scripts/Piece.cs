﻿using UnityEngine;
using System.Collections;

public class Piece : MonoBehaviour
{
    public int gridX, gridY, pieceNumber;
    public bool firstTurn = true;
    public GameObject[] PositionCubes;
    private int positionCubeItterator;
    internal string color;
    Quaternion positionCubeRotation;
    GameManagement GameManager;

    internal int GridX
    {
        get { return gridX; }
        set { gridX = value; }
    }

    internal int GridY
    {
        get { return gridY; }
        set { gridY = value; }
    }
    
    internal int PieceNumber
    {
        get { return pieceNumber; }
        set { pieceNumber = value; }
    }

    internal string Color
    {
        get { return color; }
        set { color = value; }
    }

    internal int PositionCubeItterator
    {
        get { return positionCubeItterator; }
        set { positionCubeItterator = value; }
    }

	// Use this for initialization
	void Start () {
        GameManager = GameObject.Find("GameManager").GetComponent<GameManagement>();
        positionCubeRotation = transform.rotation;    //Gets rotation of Bishop Object
        PositionCubeItterator = 0;
        PositionCubes = new GameObject[64];

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void MovePiece(int xCoord, int yCoord)
    {
        //if it it is my turn
        if(GameManagement.GetTurn() == Co.Player)
            foreach (Transform childTransform in transform) Destroy(childTransform.gameObject); //Guessing this gets rid of the cubes

        try
        {
            GameObject gameObjectToDestroy;
            gameObjectToDestroy = GameObject.Find("GameManager").GetComponent<GameManagement>().GetAllPieces(xCoord, yCoord);
            if (gameObjectToDestroy.name == "BlackKing(Clone)" || gameObjectToDestroy.name == "WhiteKing(Clone)")
            {
                if(GameManagement.GetTurn() == Co.Player)
                    GameObject.Find("GameManager").GetComponent<GameManagement>().GameOverScreen();
                else
                    GameObject.Find("GameManager").GetComponent<GameManagement>().GameOverScreen2();
            }
            gameObjectToDestroy.SetActive(false);
            gameObjectToDestroy.GetComponent<Piece>().GridX = -20;
            gameObjectToDestroy.GetComponent<Piece>().gridY = -20;
        }
        catch
        {

        }

        int difX = gridX- xCoord;
        int difY = gridY - yCoord;
        gridX = gridX - difX;
        gridY = gridY - difY;
        transform.Translate(0.0f, -difX * Co.SQUARE, difY * Co.SQUARE);

        if (GameManagement.GetTurn() == Co.Player)
        {
            string TempString = pieceNumber.ToString() + "," + gridX.ToString() + "," + gridY.ToString();
            Client.SetMessage(TempString);
        }
            

        GameManagement.SetTurn();
        GameManagement.SetAlreadyClicked(false);

    }

    public void MovePiece2(int xCoord, int yCoord)
    {

        int difX = gridX - xCoord;
        int difY = gridY - yCoord;
        gridX = gridX - difX;
        gridY = gridY - difY;
        transform.Translate(0.0f, -difX * Co.SQUARE, difY * Co.SQUARE);

    }



    public void PlacePositionCubesInRow(string Direction, int Distance, int conditional)
    {

        int xMUL = 0;
        int yMUL = 0;
        int DMX = 0;
        int DMY = 0;
        int castle = 0;
        if (Direction.Equals("NORTH"))
        {
            xMUL = 1;
            yMUL = 0;
        }
        if (Direction.Equals("SOUTH"))
        {
            xMUL = -1;
            yMUL = 0;
        }
        if (Direction.Equals("EAST"))
        {
            xMUL = 0;
            yMUL = -1;
        }
        if (Direction.Equals("WEST"))
        {
            xMUL = 0;
            yMUL = 1;
        }
        if (Direction.Equals("NORTHEAST"))
        {
            xMUL = 1;
            yMUL = -1;
        }
        if (Direction.Equals("NORTHWEST"))
        {
            xMUL = 1;
            yMUL = 1;
        }
        if (Direction.Equals("SOUTHEAST"))
        {
            xMUL = -1;
            yMUL = -1;
        }
        if (Direction.Equals("SOUTHWEST"))
        {
            xMUL = -1;
            yMUL = 1;
        }
        for (int D = 1; D < Distance; D++)
        {
            //Debug.Log("GridX" + gridX);
            //Debug.Log("GridY" + gridY);

            //check empty in direction
            if (Direction.Equals("K"))
            {
                switch (D)
                {
                    case 1:
                        DMX = 2 + gridX;
                        DMY = 1 + gridY;
                        break;
                    case 2:
                        DMX = 2 + gridX;
                        DMY = -1 + gridY;
                        break;
                    case 3:
                        DMX = 1 + gridX;
                        DMY = 2 + gridY;
                        break;
                    case 4:
                        DMX = 1 + gridX;
                        DMY = -2 + gridY;
                        break;
                    case 5:
                        DMX = -1 + gridX;
                        DMY = 2 + gridY;
                        break;
                    case 6:
                        DMX = -1 + gridX;
                        DMY = -2 + gridY;
                        break;
                    case 7:
                        DMX = -2 + gridX;
                        DMY = 1 + gridY;
                        break;
                    case 8:
                        DMX = -2 + gridX;
                        DMY = -1 + gridY;
                        break;
                    default:
                        break;
                }
                Debug.Log(DMX + "," + DMY);
            }
            else if (Direction.Equals("CASTLE"))
            {
                switch (D)
                {
                    case 1:
                        if (!GameManager.PieceExists(gridX, gridY + 1) && 
                            !GameManager.PieceExists(GridX, gridY + 2) &&
                            GameManager.GetAllPieces(GridX, GridY + 3).name.ToUpper().Equals(color + "ROOK(CLONE)")&&
                            GameManager.GetAllPieces(GridX, GridY + 3).GetComponent<Piece>().firstTurn)
                        {
                            DMX = gridX;
                            DMY = gridY + 2;
                            castle = 2;
                        }
                        else
                        {
                            DMX = gridX;
                            DMY = gridY;
                            castle = 1;
                        }
                        break;
                    case 2:
                        if (!GameManager.PieceExists(gridX, gridY - 1) &&
                            !GameManager.PieceExists(GridX, gridY - 2) && 
                            !GameManager.PieceExists(GridX, gridY - 3) && 
                            GameManager.GetAllPieces(GridX, GridY - 4).name.ToUpper().Equals(color + "ROOK(CLONE)")&&
                            GameManager.GetAllPieces(GridX, GridY - 4).GetComponent<Piece>().firstTurn)
                        {
                            Debug.Log("$");
                            DMX = gridX;
                            DMY = gridY - 2;
                        }
                        else
                        {
                            DMX = gridX;
                            DMY = gridY;
                        }

                        break;
                    default:
                        break;
                }
                Debug.Log("$" + color + "ROOK(CLONE)");
                GameObject tempGameObject = GameManager.GetAllPieces(gridX, gridY + 3);
                Debug.Log(tempGameObject.name.ToUpper());
            }
            else
            {
                DMX = D * xMUL + gridX;
                DMY = D * yMUL + gridY;
            }
            Debug.Log(DMX +"," + DMY);
            if ((DMY < 0) || (DMY > 7) || (DMX < 0) || (DMX > 7))
            {
                if ((conditional == 4))
                    continue;
                //Debug.Log("shouldbreak");
                else
                    break;
            }
                

            //if my other players pieces light them up otherwise stop progression
            if(GameManager.PieceExists(DMX, DMY))
            {
                //Debug.Log("PieceFountAt" + DMX + "," + DMY + "," + conditional);
                if (conditional == 3) //if pawn break because we can't capture a piece in front of us
                    break;
                Piece piece = GameManager.GetAllPieces(DMX, DMY).GetComponent<Piece>();
                if (!piece.Color.Equals(color))
                {
                    if (!((conditional == 5) && CheckCheck((DMX - gridX), (DMY - gridY))))
                    CreatePositionCube((DMX - gridX), (DMY - gridY), castle);
                    //Debug.Log("createdapositioncube");
                }
                if(!(conditional == 4)) //if we are a knight or a king we want to keep searching in a ring 
                    break;
            }
            else if(!(conditional==1)) //there was no piece where we are looking so put a cube and keep going
            {
                if (!((conditional == 5) && CheckCheck((DMX - gridX), (DMY - gridY))))
                CreatePositionCube((DMX - gridX), (DMY - gridY), castle);
            }
        }
    }

    public void CreatePositionCube(int x, int y, int castle)
    {
        PositionCube PositionCubeScript;
        Vector3 positionCubePosition = transform.position + new Vector3(x * Co.SQUARE, Co.SQUARE / 2, y * Co.SQUARE);
        GameManagement.SetAlreadyClicked(true);
        firstTurn = false;
        PositionCubes[PositionCubeItterator] = (GameObject)Instantiate(Resources.Load("Prefab/PositionCube"), positionCubePosition, positionCubeRotation);
        PositionCubes[PositionCubeItterator].transform.parent = transform;
        PositionCubeScript = PositionCubes[PositionCubeItterator].GetComponent<PositionCube>();
        PositionCubeScript.SetCoords((gridX + x), (GridY + y), castle);
        PositionCubeItterator++;
    }

    public bool CheckCheck(int xGrid, int yGrid)
    {
        int xposition = gridX + xGrid;
        int yposition = gridY + yGrid;
        Debug.Log("CheckingForCheck" + xposition + "," + yposition);
        for (int i = -1; i < 2; i++)
            for (int j = -1; j < 2; j++)
                for (int distance = 1; distance < 9; distance++)
                {
                    if (i == 0 && j == 0)
                    {
                        int DMX = 0;
                        int DMY = 0;
                        switch (distance)
                        {
                            case 1:
                                DMX = 2 + xposition;
                                DMY = 1 + yposition;
                                break;
                            case 2:
                                DMX = 2 + xposition;
                                DMY = -1 + yposition;
                                break;
                            case 3:
                                DMX = 1 + xposition;
                                DMY = 2 + yposition;
                                break;
                            case 4:
                                DMX = 1 + xposition;
                                DMY = -2 + yposition;
                                break;
                            case 5:
                                DMX = -1 + xposition;
                                DMY = 2 + yposition;
                                break;
                            case 6:
                                DMX = -1 + xposition;
                                DMY = -2 + yposition;
                                break;
                            case 7:
                                DMX = -2 + xposition;
                                DMY = 1 + yposition;
                                break;
                            case 8:
                                DMX = -2 + xposition;
                                DMY = -1 + yposition;
                                break;
                            default:
                                break;
                        }
                        if (GameManager.PieceExists(DMX,DMY))
                        {
                            GameObject PieceToCheck = GameManager.GetAllPieces(DMX, DMY);
                            if (!PieceToCheck.GetComponent<Piece>().color.Equals(color))
                            {
                                if (PieceToCheck.name.Contains("Knight"))
                                {
                                    Debug.Log("foundKnight" + PieceToCheck.name);
                                    return true;
                                }

                            }
                        }
                    }
                    else if (GameManager.PieceExists(i * distance + xposition, j * distance + yposition))
                    {
                        GameObject PieceToCheck = GameManager.GetAllPieces(i * distance + xposition, j * distance + yposition);
                        if (PieceToCheck.GetComponent<Piece>().Color.Equals(color))
                            break;
                        else
                        {
                            if (j == 0 || i == 0)
                            {

                                if (distance == 1)
                                {
                                    if (PieceToCheck.name.Contains("King"))
                                    {
                                        Debug.Log("foundKing" + PieceToCheck.name);
                                        return true;
                                    }

                                }
                                else if (PieceToCheck.name.Contains("Rook") || PieceToCheck.name.Contains("Queen"))
                                {
                                    Debug.Log("foundRookorQueen" + PieceToCheck.name);
                                    return true;
                                }
                                else  //hit a harmless blocking piece
                                    break;
                                    
                            }
                            else //diagonal
                            {
                                if (distance == 1)
                                {
                                    if (PieceToCheck.name.Contains("Pawn"))
                                    {
                                        if (color.Equals("BLACK"))
                                            if (PieceToCheck.GetComponent<Piece>().GridX > xposition)  //White Pieces are at the higher numbers and move down
                                                return true;
                                        if (color.Equals("WHITE"))
                                            if (PieceToCheck.GetComponent<Piece>().GridX < xposition)  //Black pieces are at the lower numbers and move up
                                                return true;
                                    }
                                    else if (PieceToCheck.name.Contains("King"))
                                    {

                                        Debug.Log("Found King" + PieceToCheck.name);
                                        return true;
                                    }
                                }
                                else if (PieceToCheck.name.Contains("Bishop") || PieceToCheck.name.Contains("Queen"))
                                {
                                    Debug.Log("Found Bishop or Queen" + PieceToCheck.name);
                                    return true;
                                }
                                else //hit a harmless blocking piece
                                    break;
                                    
                            }
                        }
                    }
                }
        Debug.Log("noCeck");
        return false;
    }
}
