﻿using UnityEngine;
using System.Collections;

public class King : Piece
{
    void OnMouseDown()
    {
        //  make position cubes
        if (((Co.Player && GameManagement.GetTurn() && color == "BLACK") || (!Co.Player && !GameManagement.GetTurn() && color == "WHITE")) && !GameManagement.GetAlreadyClicked())
        {
            if (firstTurn)
            {
                Debug.Log("Attempt Castle");
                PlacePositionCubesInRow("CASTLE", 3, 4);
            }
            PlacePositionCubesInRow("NORTHEAST", 2,5);
            PlacePositionCubesInRow("NORTHWEST",2,5);
            PlacePositionCubesInRow("SOUTHEAST",2,5);
            PlacePositionCubesInRow("SOUTHWEST",2,5);
            PlacePositionCubesInRow("NORTH",2,5);
            PlacePositionCubesInRow("SOUTH",2,5);
            PlacePositionCubesInRow("EAST",2,5);
            PlacePositionCubesInRow("WEST",2,5);
            
        }
        
    }
}

