﻿using UnityEngine;
using System.Collections;

public class Knight : Piece
{
    void OnMouseDown()
    {
        //  make position cubes
        if (((Co.Player && GameManagement.GetTurn() && color == "BLACK") || (!Co.Player && !GameManagement.GetTurn() && color == "WHITE")) && !GameManagement.GetAlreadyClicked())
        {
            PlacePositionCubesInRow("K",9,4);  //conditional 4 for circular checking don't stop
        }
    }
}

