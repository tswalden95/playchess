﻿using UnityEngine;
using System.Collections;

public class GameManagement : MonoBehaviour
{
    //variables
    Rigidbody positionRigidBody;
    public GameObject[] AllPieces;
    public GameObject popUpGameOverScreen;
    public GameObject popUpGameOverScreen2;
    static bool Turn;
    static bool AlreadyClicked = false;
    float xPosition, yPosition, zPosition;
    int i;
    int j = 0;
    Vector3 positionCubePosition;

    public GameObject GetAllPieces(int x, int y)
    {
        foreach(GameObject tempObject in AllPieces)
        {
            if (tempObject.GetComponent<Piece>().gridX == x && tempObject.GetComponent<Piece>().gridY == y)
                return tempObject;       
        }
        Debug.Log("PieceNotFound");
            return new GameObject();
    }

    public bool PieceExists(int x, int y)
    {
        foreach (GameObject tempObject in AllPieces)
        {
            if (tempObject.GetComponent<Piece>().gridX == x && tempObject.GetComponent<Piece>().gridY == y)
                return true;
        }
        return false;
    }

    public GameObject GetPiece(int j)
    {
        return AllPieces[j];
    }

    public static bool GetTurn()
    {
        return Turn;
    }

    public static void SetTurn()
    {
        if(Turn)
            Turn = false;
        else
            Turn = true;
    }

    public static bool GetAlreadyClicked()
    {
        return AlreadyClicked;
    }

    public static void SetAlreadyClicked( bool alreadyClicked)
    {
        AlreadyClicked = alreadyClicked;
    }

    // Use this for initialization
    void Start ()
    {
        Piece tempPiece;
        AllPieces = new GameObject[32];
        xPosition = 0.0f;
        yPosition = 0.0f;
        zPosition = 0.0f;
        Turn = false;
        string[] Pieces = { "Prefab/BlackRook","Prefab/BlackKnight","Prefab/BlackBishop","Prefab/BlackQueen","Prefab/BlackKing","Prefab/BlackBishop","Prefab/BlackKnight","Prefab/BlackRook",
                            "Prefab/BlackPawn","Prefab/BlackPawn","Prefab/BlackPawn","Prefab/BlackPawn","Prefab/BlackPawn","Prefab/BlackPawn","Prefab/BlackPawn","Prefab/BlackPawn",
                            "Prefab/WhitePawn","Prefab/WhitePawn","Prefab/WhitePawn","Prefab/WhitePawn","Prefab/WhitePawn","Prefab/WhitePawn","Prefab/WhitePawn","Prefab/WhitePawn",
                            "Prefab/WhiteRook","Prefab/WhiteKnight","Prefab/WhiteBishop","Prefab/WhiteQueen","Prefab/WhiteKing","Prefab/WhiteBishop","Prefab/WhiteKnight","Prefab/WhiteRook"};

        Quaternion positionCubeRotation = transform.rotation;

        //placeBlack Rooks

        zPosition = 0.0f;
        for(i=0;i<32;i++)
        {
            positionCubePosition = transform.position + new Vector3(xPosition, yPosition, zPosition);
            AllPieces[i] = (GameObject)Instantiate(Resources.Load(Pieces[i].ToString()), positionCubePosition, positionCubeRotation);
            //Debug.Log(AllPieces[i].name);
            tempPiece = AllPieces[i].GetComponent<Piece>();
            zPosition += Co.SQUARE;
            tempPiece.GridY = i % 8;
            if (i == 7 || i == 23)
            {
                zPosition = 0.0f;
                xPosition += Co.SQUARE;
            }
            if(i == 15)
            {
                zPosition = 0.0f;
                xPosition += Co.SQUARE * 5;
            }
            if (i < 8)
                tempPiece.GridX = 0;
            else if (i < 16)
                tempPiece.GridX = 1;
            else if (i < 24)
                tempPiece.GridX = 6;
            else if (i < 32)
                tempPiece.GridX = 7;
            if (i < 16)
                tempPiece.Color = "BLACK";
            else
                tempPiece.Color = "WHITE";
            tempPiece.PieceNumber = i;       
        }
    }

    public int GetPieceX(int pieceEnumerator)
    {
        return AllPieces[pieceEnumerator].GetComponent<Piece>().gridX;
    }

    public int GetPieceY(int pieceEnumerator)
    {
        return AllPieces[pieceEnumerator].GetComponent<Piece>().gridY;
    } 

    public void GameOverScreen()
    {
        popUpGameOverScreen.SetActive(true);
    }
    public void GameOverScreen2()
    {
        popUpGameOverScreen2.SetActive(true);
    }
}
